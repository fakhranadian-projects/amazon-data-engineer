 1. Upload the local files data into s3 bucket
 2. Read the data with gzip and loop the data into chunk to avoid overload
 3. Select necessary column for smaller size in database
 4. Insert the data into aws-rds


Normalize data:
Column that we take:
    - asin              = Amazon Standard Identification Number (ID)
    - title             = the product name
    - category          = category of product
    - brand             = brand of product
    - price             = price of product
    - description       = description of product
    - imageURLHighRes   = image link


Column that's available in the data:

{"category": ["Video Games", "PC", "Games"], 
"tech1": "", 
"description": [], 
"fit": "", 
"title": "Reversi Sensory Challenger", 
"also_buy": [], 
"tech2": "", 
"brand": "Fidelity Electronics", 
"feature": [], 
"rank": [">#2,623,937 in Toys &amp; Games (See Top 100 in Toys &amp; Games)", ">#39,015 in Video Games &gt; PC Games"], 
"also_view": [], 
"main_cat": "Toys &amp; Games", 
"similar_item": "", 
"date": "", 
"price": "", 
"asin": "0042000742", 
"imageURL": ["https://images-na.ssl-images-amazon.com/images/I/31nTxlNh1OL._SS40_.jpg"], 
"imageURLHighRes": ["https://images-na.ssl-images-amazon.com/images/I/31nTxlNh1OL.jpg"]}