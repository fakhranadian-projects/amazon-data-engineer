DROP TABLE IF EXISTS amazon_review_dwh;
CREATE TABLE amazon_review_dwh(
    review_id serial primary key,
	overall int,
    "reviewerID" varchar,
    asin varchar,
    "reviewerName" varchar,
    "reviewText" varchar,
    summary varchar,
    "unixReviewTime" bigint,
    vote varchar
)
