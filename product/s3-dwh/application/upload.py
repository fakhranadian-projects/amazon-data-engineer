from .conn import ConnectionDatabase

class UploadFile:

    def upload_data(self):

        # define connection to s3 bucket
        s3 = ConnectionDatabase.conn_s3(self)

        try:
            # upload local json file into s3 bucket
            
            file_name       = 'meta_Video_Games.json.gz'

            bucket_name     = 'bucket-january-fakhran'

            s3.upload_file(file_name, bucket_name, file_name)
            
            print('Success insert to s3 bucket')
        
        except Exception as e:

            print('Failed insert to s3 bucket', e)