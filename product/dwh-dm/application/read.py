"""
    author: fakhran adian
"""

import pandas as pd, yagmail, os #type: ignore
from .conn import ConnectionDatabase


class ReadData:

    def read_insert(self):

        # define connection to database
        con_database    = ConnectionDatabase.conn_aws(self)
        
        # validate junk dimension
        df_scd          = pd.read_sql("""
                            SELECT status 
                            FROM etl_system 
                            WHERE etlname='Final-Project-Product-ETL-2'
                        """, con=con_database)
        
        first_value     = df_scd['status'][0]

        validasi_data   = 'N'

        if first_value != validasi_data:

            try:
                # read data from aws-rds
                query = """
                                        SELECT DISTINCT(asin) AS product_id,

                                            trim(regexp_replace(title, '\s+', ' ', 'g')) AS product_name,

                                            replace(regexp_replace(split_part(category, ',', 2), '[^\w& ]', '', 'g'), 'amp ', ' ') AS category,

                                            CASE WHEN position('by' in brand) = 1 THEN trim(regexp_replace(substring(brand from 3), '\s+', ' ', 'g'))

                                            ELSE trim(regexp_replace(replace(brand, 'by', ''), '\s+', ' ', 'g')) END AS brand,

                                            CASE WHEN regexp_replace(price, '[$,]', '', 'g') ~ '^\d+(\.\d{1,2})?$'

											THEN regexp_replace(price, '[$,]', '', 'g')::float ELSE NULL END AS price,

                                            regexp_replace(regexp_replace(description, '[{"}]', '', 'g'), '\s+', ' ', 'g') AS description,

                                            regexp_replace("imageURLHighRes", '[{}]', '', 'g') AS image_link
                                            
                                        FROM amazon_product_dwh

                                            WHERE LENGTH(title) != 0
                                                    """
                
                df = pd.read_sql(query, con=con_database)  
          
                # Insert data into aws-rds dwh
                df.to_sql('amazon_product_dm',con=con_database, if_exists='append', index=False)

                print('Success read and insert to aws-rds dwh')


            except Exception as e:

                file    = open('./log_etl/error_log_etl.txt', 'w')

                file.write('Error ETL DWH-DM Product: %s' % e)
                
                file.close()
                
                # email sender and token apps
                gmail   = yagmail.SMTP('fakhran.adian@gmail.com', 'eaajnbqjtokkmsjb')

                # send email
                gmail.send('fakhran.g2academy@gmail.com', 
                           
                        subject='ETL-Error-DWH-DM-Amazon-Product', 

                        contents='Dear All there is ETL error, Please check',
                        
                        attachments='./log_etl/error_log_etl.txt'
                        )
                
                # remove log from local folder
                os.remove('./log_etl/error_log_etl.txt')

                print('Cannot read and insert to aws-rds dwh', e)
                
