This ETL mainly to normalize and clean the data as explained below:

Normalize data:
1. Column name normalize into:
    - review_id       = review_id
    - asin            = product_id
    - reviewerID      = reviewer_id
    - reviewerName    = reviewer_name
    - unixReviewTime  = reviewed_at
    - overall         = rating
    - vote            = voted_helpfull
    - summary         = summary
    - reviewText      = review
    
2. Data Manipulation
    - reviewer_name  = trim(regexp_replace("reviewerName", '\s+', ' ', 'g')) as reviewer_name
                        --> remove any extra spaces 

    - reviewed_at      = to_timestamp("unixReviewTime")::date as reviewed_at 
                        --> convert unixReviewTime into date dtype

    - voted_helpfull  = COALESCE(CAST(replace(vote, ',', '') AS numeric)::integer, 0) as voted_helpfull
                        --> remove ',' because it makes the data can't be converted into integer
                        --> convert data type into integer, and then replace null value with 0

    - summary        = trim(regexp_replace(summary, '\s+', ' ', 'g')) as summary
                        --> remove any extra spaces 
                        
    - review         = trim(regexp_replace("reviewText", '\s+', ' ', 'g')) as review	
                        --> remove any extra spaces \
                        