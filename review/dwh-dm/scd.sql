DROP TABLE IF EXISTS amazon_review_dm;
CREATE TABLE amazon_review_dm(
    review_id serial primary key,
    product_id varchar,
    reviewer_id varchar,
    reviewer_name varchar,
    reviewed_at date,
    rating int,
    voted_helpfull int,
    summary varchar,
    review varchar
)

