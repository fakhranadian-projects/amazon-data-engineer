"""
    author: fakhran adian
"""

import pandas as pd, yagmail, os #type: ignore
from .conn import ConnectionDatabase


class ReadData:

    def read_insert(self):

        # define connection to database
        con_database    = ConnectionDatabase.conn_aws(self)
        
        # validate junk dimension
        df_scd          = pd.read_sql("""
                            SELECT status 
                            FROM etl_system 
                            WHERE etlname='Final-Project-Fact-Table-ETL'
                        """, con=con_database)
        
        first_value     = df_scd['status'][0]

        validasi_data   = 'N'

        if first_value != validasi_data:

            try:
                # read data from aws-rds
                query   = """
                                        WITH r AS (
                                                    SELECT
                                                        product_id,

                                                        ROUND(AVG(rating), 2) AS product_rating,

                                                        COUNT(rating) AS product_sold

                                                    FROM amazon_review_dm

                                                    GROUP BY product_id
                                                    )

                                        SELECT
                                            p.product_id,
                                            p.product_name,

                                            CASE

                                                WHEN p.category = '' THEN 'Uncategorized'
                                                ELSE p.category

                                            END AS category,

                                            r.product_rating,
                                            r.product_sold,
                                            p.price
                                            
                                        FROM amazon_product_dm AS p

                                        JOIN r ON r.product_id = p.product_id
                                                                                """
                
                df      = pd.read_sql(query, con=con_database)

                # Insert data into aws-rds dwh
                df.to_sql("amazon_fact_table", con=con_database, if_exists="append",index=False)

                print('Success read and insert to aws-rds dwh')


            except Exception as e:

                file    = open('./log_etl/error_log_etl.txt', 'w')

                file.write('Error ETL Fact-Table: %s' % e)
                
                file.close()
                
                # email sender and token apps
                gmail   = yagmail.SMTP('fakhran.adian@gmail.com', 'eaajnbqjtokkmsjb')

                # send email 
                gmail.send('fakhran.g2academy@gmail.com', 
                           
                        subject='ETL-Error-Amazon-Fact-Table', 

                        contents='Dear All there is ETL error, Please check',
                        
                        attachments='./log_etl/error_log_etl.txt'
                        )
                
                # remove log from local folder
                os.remove('./log_etl/error_log_etl.txt')

                print('Cannot read and insert to aws-rds dwh', e)
                
