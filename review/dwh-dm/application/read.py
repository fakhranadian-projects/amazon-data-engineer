"""
    author: fakhran adian
"""

import pandas as pd, yagmail, os #type: ignore
from .conn import ConnectionDatabase


class ReadData:

    def read_insert(self):
        
        # define connection to database
        con_database    = ConnectionDatabase.conn_aws(self)
        
        # validate junk dimension
        df_scd          = pd.read_sql("""
                            SELECT status 
                            FROM etl_system 
                            WHERE etlname='Final-Project-Review-ETL-2'
                        """, con=con_database)
        
        first_value     = df_scd['status'][0]

        validasi_data   = 'N'

        if first_value != validasi_data:

            try:
                # read data from aws-rds
                query   = """
                                        SELECT review_id, asin as product_id,

                                                "reviewerID" as reviewer_id,
                                        
                                                trim(regexp_replace("reviewerName", '\s+', ' ', 'g')) as reviewer_name,
                                        
                                                to_timestamp("unixReviewTime")::date as reviewed_at,

                                                overall as rating,

                                                COALESCE(CAST(replace(vote, ',', '') AS numeric)::integer, 0) as voted_helpfull,

                                                trim(regexp_replace(summary, '\s+', ' ', 'g')) as summary,
                                                
                                                trim(regexp_replace("reviewText", '\s+', ' ', 'g')) as review		
                                        
                                        FROM amazon_review_dwh     """
                
                df      = pd.read_sql(query, con=con_database)
                    
                # drop duplicated data
                df_dm   = df.drop_duplicates(subset=["reviewer_id", "product_id", "review"], keep=False)

                # Insert data into aws-rds dwh
                df_dm.to_sql("amazon_review_dm", con=con_database, if_exists="append",index=False)

                print('Success read and insert to aws-rds dwh')


            except Exception as e:

                file    = open('./log_etl/error_log_etl.txt', 'w')

                file.write('Error ETL DWH-DM Review: %s' % e)
                
                file.close()
                
                # email sender and token apps
                gmail   = yagmail.SMTP('fakhran.adian@gmail.com', 'eaajnbqjtokkmsjb')

                # send email
                gmail.send('fakhran.g2academy@gmail.com', 
                           
                        subject='ETL-Error-DWH-DM-Amazon-Review', 

                        contents='Dear All there is ETL error, Please check',
                        
                        attachments='./log_etl/error_log_etl.txt'
                        )
                
                # remove log from local folder
                os.remove('./log_etl/error_log_etl.txt')

                print('Cannot read and insert to aws-rds dwh', e)
                
