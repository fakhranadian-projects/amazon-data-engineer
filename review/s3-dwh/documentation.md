 1. Upload the local files data into s3 bucket
 2. Read the data with gzip and loop the data into chunk to avoid overload
 3. Select necessary column for smaller size in database
 4. Insert the data into aws-rds

Normalize data:
Column that we take:
    review_id   = auto-generated id
    asin - ID of the product
    reviewerID  = ID of the reviewer
    reviewerName - name of the reviewer
    overall - rating of the product
    vote - helpful votes of the review
    summary - summary of the review
    reviewText - text of the review
    unixReviewTime - time of the review (unix time)


Column that's available in the data:

"overall": 5.0, (rating)
"verified": true, 
"reviewTime": "10 6, 2015", (reviewed_at)
"reviewerID": "A27GRRN4CL4W2E", (reviewer_id)
"asin": "6050036071", (product_id)
"reviewerName": "George", (reviewer_name)
"reviewText": "I received it quickly and it works great!", (review_text)
"summary": "Five Stars", (summary)
"unixReviewTime": 1444089600 (unix_review_time)
