CREATE TABLE etl_system(
	id integer primary key,
	etlname varchar,
	status varchar)
	
INSERT INTO etl_system
VALUES
		(1, 'Final-Project-Product-ETL-1','Y'),
		(2, 'Final-Project-Review-ETL-1','Y'),
		(3, 'Final-Project-Product-ETL-2','Y'),
		(4, 'Final-Project-Review-ETL-2','Y'),
		(5, 'Final-Project-Fact-Table-ETL','Y')