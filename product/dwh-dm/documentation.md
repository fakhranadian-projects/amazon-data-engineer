This ETL mainly to normalize and clean the data as explained below:

Normalize data:
1. Column name normalize into:
    - asin              = product_id
    - title             = product_name
    - category          = category
    - brand             = brand
    - price             = price
    - description       = description
    - imageURLHighRes   = image_link

2. Data Manipulation
    - product_id    = DISTINCT(asin) AS product_id
                        --> asin(Amazon Standard Identification Number), then distinct because there is a duplicate

    - product_name  = trim(regexp_replace(title, '\s+', ' ', 'g')) AS product_name
                        --> remove any extra spaces 

    - category  = replace(regexp_replace(split_part(category, ',', 2), '[^\w& ]', '', 'g'), 'amp ', ' ') AS category
                        --> split part to get the character after comma to get the category
                        --> remove any special character and then replace 'amp' because of unknown character

    - brand         = CASE WHEN position('by' in brand) = 1 THEN trim(regexp_replace(substring(brand from 3), '\s+', ' ', 'g'))
                      ELSE trim(regexp_replace(replace(brand, 'by', ''), '\s+', ' ', 'g')) END AS brand
                        --> detect every character that starts with 'by' (when checked there's a lot 'by' that duplicate with the data without the 'by')
                        --> remove the 'by' and then remove extra spaces available, and then trim extra space before and after text

    - price         = CASE WHEN regexp_replace(price, '[$,]', '', 'g') ~ '^\d+(\.\d{1,2})?$'
                      THEN regexp_replace(price, '[$,]', '', 'g')::float ELSE NULL END AS price
                        --> Find a string that starts with one or more digits, followed by an optional decimal point and one or two more digits.
                        --> For example, it matches strings like 10, 10.5, and 10.55, but not strings like $10 or 10.555.
                        --> Then if it matches, remove the dollar sign and convert the data into float, else give the data a NULL

    - description   = regexp_replace(regexp_replace(description, '[{"}]', '', 'g'), '\s+', ' ', 'g') AS description
                        --> remove any extra spaces and then remove the curly braces and quotation marks outside the text

    - image_link    = regexp_replace("imageURLHighRes", '[{}]', '', 'g') AS image_link
                        --> remove all the curly braces outside the text
                        
    - WHERE CLAUSE  = WHERE LENGTH(title) != 0
                        --> unselect the product_name that is not defined with length != 0


