from sqlalchemy import create_engine #type: ignore
from dotenv import load_dotenv #type: ignore
import os #type: ignore

class ConnectionDatabase:
    
    def conn_aws(self):

        load_dotenv()

        URL     = os.environ['urlaws']
        USER    = os.environ['usraws']
        PASS    = os.environ['pasaws']
        PORT    = os.environ['poraws']
        DB      = os.environ['dbaws']

        engine_one  = create_engine("postgresql://{USER}:{PASS}@{URL}:{PORT}/{DB}".format(
                                    USER=USER,
                                    URL=URL,
                                    PASS=PASS,
                                    DB=DB,
                                    PORT=PORT
                                    ))
        return engine_one


   