# Amazon Product and Reviews Data Engineering Projects
![Amazon](/amazon.jpg)

## Data Architecture
![Data Architecture](/Data_Architecture.png)

## Overview
This project is about Amazon Products and Reviews end-to-end data pipeline, where we simulate how data flow from local files - s3 bucket - data warehouse - into user usage such as Data Visualization and Machine Learning API later in Data Science projects.

Several tools and programming language used in this project:
- Python
- PostgreSQL
- AWS S3 Bucket
- AWS-RDS
- Apache Airflow

## ETL Flow
- Data collected from amazon within this link : [Amazon Datasets](https://cseweb.ucsd.edu/~jmcauley/datasets/amazon_v2/)
- There are 2 data that we have to upload into s3 bucket, which is amazon video games product and review as i already uploaded those json files in gzip formats.
- After uploading into s3 bucket, we will take necessary data only and chunks it into smaller rows of data then proceed to ETL the data into AWS-RDS data warehouse.
- We'll then normalize and full clean the data and insert it into data mart.
- Creating a fact table for measurement that we can use in data visualization later for business requirements.
- Data mart will be used later for my Data Science projects that focuses on Sentiment Analysis.
- Apache Airflow is used in this projects and the DAG's script is available in the folder.

## Environment and Database Setup
### ETL System
First of all you have to create a table in your postgresql database with the format in etl_system.sql to simulate a validation system for etl, you can just do this by copying all text inside the etl_system.sql and query it into your database.

### scd.sql
To avoid auto-generate tables using python etl, i like to create the table manually into the database so that we can declare what type of data that we wanted to insert, so you can just copy and paste the scd.sql files into the query tools on your postgresql database.

### Error ETL Log Email
For the ETL Error Log you have to create token apps for your email password within this link:
https://myaccount.google.com/apppasswords

You can select app 'Mail' and select device 'Windows Computer'. After that you can change the email of the sender and the token apps for the password from the link above, and then change the email that you wanted to send for the logs in the read.py files.

I have this log error handling on every application folder for every etl in the read.py files, so it might take some times to change every single email and tokens if you wanted to use this feature. Alternatively you can just delete the error logs and change it into pass.

### .env file key into database
In every application folder on every ETL process, there's .env file contains key for database access, you can change the key into your own database or just simply change it into local database e.g below:

```
#This is my username, password, and db name for my local postgresql database, you can change it into your own database
#localhost
urlaws = "localhost"
usraws = "postgres"
pasaws = "postgres"
poraws = "5432"
dbaws  = "postgres"

# secret key dan access key s3 bucket
key = 'YOUR_S3_SECRET_KEY'
acc = 'YOUR_S3_ACCESS_KEY'
reg = 'YOUR_S3_REGION'
```

## Contact
Please feel free to contact me if you have any questions. [Fakhran Adian](https://www.linkedin.com/in/fakhranadian/)
