DROP TABLE IF EXISTS amazon_product_dm;
CREATE TABLE amazon_product_dm(
    product_id varchar primary key,
    product_name varchar,
    category varchar,
    brand varchar,
    price float,
    description varchar,
    image_link varchar
)

