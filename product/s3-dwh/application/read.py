"""
    author: fakhran adian
"""

import pandas as pd, gzip, yagmail, os #type: ignore
from .conn import ConnectionDatabase

class ReadData:

    def read_insert(self):
        
        # define connection to database and s3 bucket
        s3 = ConnectionDatabase.conn_s3(self)
        con_database    = ConnectionDatabase.conn_aws(self)
        
        # validate junk dimension
        df_scd          = pd.read_sql("""
                            SELECT status 
                            FROM etl_system 
                            WHERE etlname='Final-Project-Product-ETL-1'
                        """, con=con_database)
        
        first_value     = df_scd['status'][0]

        validasi_data   = 'N'

        if first_value != validasi_data:

            try:
                
                # open gzip file using 'rb' mode from s3 bucket
                with gzip.open(s3.get_object(Bucket='bucket-january-fakhran', Key='meta_Video_Games.json.gz')['Body'],'rb') as f:
                    
                    # iterate over the chunks of data
                    for chunk in pd.read_json(f, lines=True, chunksize=10000):
                        
                        # select necessary columns from the chunk of data
                        df_dwh = chunk[['category','description','title','brand','price','asin','imageURLHighRes']]
                        
                        # insert the chunk of data into the database
                        df_dwh.to_sql('amazon_product_dwh', con=con_database, if_exists='append', index=False)
                        
                        print(f'Successfully inserted {len(chunk)} rows into the database.')

                print('Success connect and insert to aws-rds dwh')


            except Exception as e:

                file    = open('./log_etl/error_log_etl.txt', 'w')

                file.write('Error ETL S3-DWH Product: %s' % e)
                
                file.close()
                
                # email sender and token apps
                gmail   = yagmail.SMTP('fakhran.adian@gmail.com', 'eaajnbqjtokkmsjb')

                # send email
                gmail.send('fakhran.g2academy@gmail.com', 
                           
                        subject='ETL-Error-S3-DWH-Amazon-Product', 

                        contents='Dear All there is ETL error, Please check',
                        
                        attachments='./log_etl/error_log_etl.txt'
                        )
                
                # remove log from local folder
                os.remove('./log_etl/error_log_etl.txt')

                print('Cannot connect and insert to aws-rds dwh', e)
        
