# Copy this Dags into your airflow, but don't forget to change the bash_command directory into your own directory

from airflow.operators.bash_operator import BashOperator
from airflow import DAG
from datetime import datetime
import pendulum

local_tz = pendulum.timezone("Asia/Jakarta")

args = {'owner':'airflow'}

dag1 = DAG(
    'final-project-de-etl-fact-table',
    start_date = datetime(2023, 5, 1, tzinfo=local_tz),
    schedule_interval = '0 3 1 * *',
    default_args = args
)

etl1 = BashOperator(
                task_id= 'final-project-fact-table-etl',
                bash_command= 'python3 /home/linux/handson/de/FINAL-PROJECT/fact-table/main.py',
                dag=dag1
)

etl1
