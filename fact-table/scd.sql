DROP TABLE IF EXISTS amazon_fact_table;
CREATE TABLE amazon_fact_table(
    id serial primary key,
    product_id varchar,
    product_name varchar,
    category varchar,
    product_rating float,
    product_sold int,
    price float
)