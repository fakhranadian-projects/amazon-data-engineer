DROP TABLE IF EXISTS amazon_product_dwh;
CREATE TABLE amazon_product_dwh(
    category varchar,
    description varchar,
    title varchar,
    brand varchar,
    price varchar,
    asin varchar,
    "imageURLHighRes" varchar
)

