pandas == 1.5.1
sqlalchemy == 1.4.46
boto3 == 1.26.79
python-dotenv == 0.21.1
yagmail == 0.15.293